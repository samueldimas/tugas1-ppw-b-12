function comment() {
    event.preventDefault(true)
    let token = $('input[name="csrfmiddlewaretoken"]').attr('value');

    $.ajax({
        method: 'POST',
        url: 'http://localhost:8000/about/',
        datatype: 'json',
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token);
        },
        success: function(result) {
            console.log(result);
        }
    })
}