from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Event(models.Model):
    name = models.CharField(max_length=50)
    date = models.DateTimeField()
    place = models.CharField(max_length=50)
    description = models.TextField(max_length=500)
    photo = models.URLField(null=True, blank=True, default='')
    google_member_participants = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return self.name