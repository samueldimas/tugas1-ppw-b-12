from django.test import TestCase, Client
from django.urls import resolve
from .views import event
from .models import Event

# Create your tests here.
class EventUrlTests(TestCase):
    def test_join_event_url_is_exist(self):
        response = Client().get('/event/')
        self.assertEqual(response.status_code, 200)

    def test_landing_using_landing_func(self):
        found = resolve('/event/')
        self.assertEqual(found.func, event)

class EventModelTest(TestCase):
    def test_model_can_create_new_event(self):
        test = "Anon"
        new_joiner = Event.objects.create ( 
            name = test,
            date = '2018-12-01 15:00:00',
            place = test,
            description = test,
            photo = 'https://media.wired.com/photos/598e35fb99d76447c4eb1f28/master/pass/phonepicutres-TA.jpg'
        )
        counting_all_available_new_event = Event.objects.all().count()
        self.assertEqual(counting_all_available_new_event, 1)