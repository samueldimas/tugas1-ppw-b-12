from django.shortcuts import render
from .models import Event

# Create your views here.
def event(request):
    event = Event.objects.all()
    response = {"event":event}
    return render(request, 'event.html', response)

#def directEvent(request):
#	return render(request, 'login.html', response)