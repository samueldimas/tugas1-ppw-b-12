# Task 1 WDP B Class B - Group 12
This Gitlab repository is the result of the work from Group 12 WDP Class B

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/samueldimas/tugas1-ppw-b-12/badges/master/pipeline.svg)](https://gitlab.com/samueldimas/tugas1-ppw-b-12/commits/master)
[![coverage report](https://gitlab.com/samueldimas/tugas1-ppw-b-12/badges/master/coverage.svg)](https://gitlab.com/samueldimas/tugas1-ppw-b-12/commits/master)

## Heroku
Click [here](http://tugas1-ppw-b-12.herokuapp.com) to see our group's website.

## Group Member
- Jasmine Nur Ariij - 1706023712
- Sandi Bhirama - 1706043563
- Ardita Sophi Ayustine - 1706043701
- Samuel Dimas Partogi - 1706074915

## Acknowledgements
- Course: WDP 2018 Gasal
- Lecturer: Ibu Maya Retno Ayu Setyautami S.Kom., M.Kom.
- Class B Teaching Assistant