from django.contrib import admin
from django.http import HttpResponse
from django.urls import path
from JoinMember import views as MemberViews

app_name = 'JoinMember'

urlpatterns = [
    path('', MemberViews.CreateMember, name='CreateMember'),
]
