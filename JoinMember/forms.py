from django import forms
from .models import Member
from django.forms import ModelForm

class add_member(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    birthdate = forms.DateField()
    class Meta:
        model = Member
        fields = ["name","username","email","birthdate","adress","password"]