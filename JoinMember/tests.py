from django.test import TestCase, Client
from django.urls import resolve
from .views import CreateMember
from .models import Member
from .forms import add_member

class testJoinMember(TestCase):
    
    def test_JoinMember_url_is_exist(self):
        response = Client().get('/join/')
        self.assertEqual(response.status_code,200)

    def test_JoinMember_using_addStatus_func(self):
        found = resolve('/join/')
        self.assertEqual(found.func,CreateMember)

    def test_JoinMember_using_template(self):
        response = Client().get('/join/' )
        self.assertTemplateUsed(response, 'JoinMember.html')

    def test_JoinMember_created(self):
        contoh_name =  "ini nama"
        contoh_username = "ini username"
        contoh_email = "iniemail@email.com"
        contoh_birthdate = "1999-10-10"
        contoh_adress = "ini adress"
        contoh_password = "inipassword"
        early_count = Member.objects.count()
        status_test = Member.objects.create(name = contoh_name, username = contoh_username, email = contoh_email, birthdate = contoh_birthdate, adress = contoh_adress, password = contoh_password)
        last_count = Member.objects.count()
        self.assertEqual(early_count + 1, last_count)  
        self.assertEqual(status_test.name, contoh_name)
        self.assertEqual(status_test.username, contoh_username)
        self.assertEqual(status_test.email, contoh_email)
        self.assertEqual(status_test.birthdate, contoh_birthdate)
        self.assertEqual(status_test.adress, contoh_adress)
        self.assertEqual(status_test.password, contoh_password)

    def test_string_representation(self):
        string = Member(name="ngoding kuy")
        self.assertEqual(str(string), string.name)

    def test_JoinMember_post_error_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/join/', {'name': '', 'username': '', 'email':'', 'birthdate': '', 'adress':'', 'password':''})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/join/')
            html_response = response.content.decode('utf8')
            self.assertNotIn(test, html_response)




