from django.shortcuts import render
from .forms import add_member
from .models import Member
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.http import HttpResponse

def CreateMember(request):
	if request.method == "POST":
		form = add_member(request.POST)
		if form.is_valid():
			form_item = form.save(commit=True)
			form_item.save()
		return HttpResponseRedirect('/join')
	else:
		form = add_member()
	return render(request, 'JoinMember.html', {'form' : form})