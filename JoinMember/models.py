from django.db import models

class Member(models.Model):
	name = models.CharField(max_length=50)
	username = models.CharField(max_length=50, unique=True)
	email = models.CharField(max_length=50, unique=True)
	birthdate = models.DateField()
	adress = models.CharField(max_length=100)
	password = models.CharField(max_length=50)

	def __str__(self):
		return "{}".format(self.name)


