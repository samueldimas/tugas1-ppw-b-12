from django.shortcuts import render, redirect
from .models import Join
from .forms import JoinForm

# Create your views here.
def join(request):
    if request.method == 'POST':
        form = JoinForm(request.POST)
        if form.is_valid():
            join = Join()
            join.username = form.cleaned_data['username']
            join.password = form.cleaned_data['password']
            join.email = form.cleaned_data['email']
            join.save()
        return redirect('/')
    else:
        form = JoinForm()
        return render(request, 'join.html', {'form': form})