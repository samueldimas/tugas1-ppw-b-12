from django.urls import path
from . import views

app_name = 'JoinEvent'

urlpatterns = [
    path('', views.join, name='join')
    
]
