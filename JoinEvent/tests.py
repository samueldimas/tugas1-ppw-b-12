from django.test import TestCase, Client
from django.urls import resolve
from .views import join
from .models import Join
from .forms import JoinForm

# Create your tests here.
class JoinEventUrlTests(TestCase):
    def test_join_event_url_is_exist(self):
        response = Client().get('/joinevent/')
        self.assertEqual(response.status_code, 200)

    def test_landing_using_landing_func(self):
        found = resolve('/joinevent/')
        self.assertEqual(found.func, join)

class JoinEventModelTest(TestCase):
    def test_model_can_create_new_news(self):
        new_joiner = Join.objects.create(
            username="test",
            password="test",
            email="test@test.com"
        )
        counting_all_available_new_joiner = Join.objects.all().count()
        self.assertEqual(counting_all_available_new_joiner, 1)
    
class JoinEventViewTest(TestCase):
    def test_join_event_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/joinevent/', {'username': test, 'password': test, 'email': test})
        self.assertEqual(response_post.status_code, 302)

    def test_form_validation_for_blank_items(self):
        form = JoinForm(data = {'username': '', 'password': '', 'email': '',})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'],
            ["This field is required."]
        )

