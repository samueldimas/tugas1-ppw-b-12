from django.db import models

# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=300)
    description = models.TextField()
    date = models.DateTimeField()

    class Meta:
        ordering = ('-date',)
        verbose_name_plural = "news"
