from django.test import TestCase, Client
from django.urls import resolve
from .views import landing, news
from .models import News

class LandingUrlTest(TestCase):
    def test_landing_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_using_landing_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)

class NewsUrlTest(TestCase):
    def test_news_url_is_exist(self):
        response = Client().get('/news')
        self.assertEqual(response.status_code, 200)

    def test_news_using_news_func(self):
        found = resolve('/news')
        self.assertEqual(found.func, news)

class NewsModelTest(TestCase):
    def test_model_can_create_new_news(self):
        new_news = News.objects.create(
            title="test",
            description="test",
            date="2020-11-01 20:00:00"
        )
        counting_all_available_news = News.objects.all().count()
        self.assertEqual(counting_all_available_news, 1)

class LandingContentTest(TestCase):
    def test_landing_contains_text(self):
        response = Client().get('/')
        self.assertContains(response, 'Student Union')
        self.assertContains(response, 'is a non-profit organization')
        self.assertContains(response, 'run by students from Faculty of Computer Science')
        self.assertContains(response, 'that accommodate talents and interest of students')
