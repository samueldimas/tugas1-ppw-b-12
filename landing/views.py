from django.shortcuts import render
from .models import News

# Create your views here.
def landing(request):
    return render(request, 'landing.html')

def news(request):
    news = News.objects.all()
    response = {"news": news}
    return render(request, 'news.html', response)