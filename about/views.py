from django.shortcuts import render, redirect
from django.http import JsonResponse
from .forms import CommentForm
from .models import Comment

# Create your views here.
def about(request):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = Comment()
            comment.comment = form.cleaned_data["comment"]
            comment.save()
        return redirect("/about/")
    else:
        form = CommentForm()
        comments = Comment.objects.all()
        return render(request, 'about.html', {'form': form, 'comments': comments})