from django.test import TestCase, Client
from django.urls import resolve
from .views import about
from .models import Comment

# Create your tests here.
class CommentTest(TestCase):
    def test_about_page_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_page_using_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)
    
    def test_model_can_create_new_comment(self):
        Comment.objects.create(
            comment="This is a dummy comment"
        )
        counting_all_available_comments = Comment.objects.all().count()
        self.assertEqual(counting_all_available_comments, 1)
    
    def test_post_comment_ajax(self):
        Client().post("/about", {"comment": "This is a dummy comment"})
        response = Client().post("/about", {"comment": "This is a dummy comment"})
        self.assertEqual(response.status_code, 301)