from django.db import models

# Create your models here.
class Comment(models.Model):
    comment = models.CharField(max_length = 200)

    class Meta:
        verbose_name_plural = "comments"
