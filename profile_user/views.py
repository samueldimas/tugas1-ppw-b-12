from django.shortcuts import render
from event.models import Event
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth.models import User

# Create your views here.
def profile(request):
    if(request.user.is_authenticated):
        regis_event = Event.objects.filter(google_member_participants=request.user)
        count = regis_event.count()
        return render(request, 'profile.html', {"regis_event": regis_event, "count": count})
    return render(request, 'profile.html', {'count':0})
    
def get_events(request):
    if(request.user.is_authenticated):
        all_events = {}
        for event in Event.objects.filter(google_member_participants=User.objects.get(username=request.user.username)):
            all_events =  {'name': event.name,
                            'description': event.description,
                            'date': event.date.strftime("%Y-%m-%d")}
        return JsonResponse({'data': all_events})
    return HttpResponseRedirect('/login/')
