from django.urls import path
from .views import profile, get_events
app_name = 'profile_user'

urlpatterns = [
    path('', profile, name = 'profile'),
    path('get-events/', get_events, name = 'get_events'),
]