from django.test import TestCase, Client
from django.urls import resolve
from .views import profile
from django.contrib.auth.models import User
from event.models import Event

# Create your tests here.
class ProfileUnitTest(TestCase):
    def test_profile_user_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)


    def test_profile_user_using_correct_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)
    
    def test_profile_user_using_correct_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_user_page(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf-8')
        self.assertNotIn("None", html_response)

    def test_registered_events(self):
        user = User.objects.create_user(username='testuser', password='abc1')
        login = self.client.login(username='testuser', password='abc1')
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)





   
